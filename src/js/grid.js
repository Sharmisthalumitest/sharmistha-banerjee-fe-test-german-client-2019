//initialization og variables
var gridViewHtml = '';
var initialVal = 0;
var showperscroll = 10;
var productdata = new Object();
var gridorList = 'grid';
$('#grid').addClass('disabled');
//function to append data in HTML
function loadresult(data){
    
    gridViewHtml = '';
    if(showperscroll < data.length){
        for (var i = initialVal; i < showperscroll; i++) {
            if(gridorList == 'grid'){
                gridViewHtml += '<div class="item col-xs-4 col-lg-4 grid-group-item">';
                 gridViewHtml += '<div class="thumbnail card">'; 
            }
            
            else{
                gridViewHtml += '<div class="item col-xs-4 col-lg-4 list-group-item ">';
                gridViewHtml += '<div class="thumbnail card width100">'; 
            }
            
            gridViewHtml += '<div class="img-event">';            
            gridViewHtml += '<img class="group list-group-image img-fluid" src="' + data[i].picture + '" alt="Product image">';
            if(data[i].isFav === true)
                gridViewHtml += '<span class="fa fa-heart circle-icon checked favIcon"></span>';
            else
                gridViewHtml += '<span class="far fa-heart  checked circle-icon favIcon"></span>';
             gridViewHtml += '</div>';
            gridViewHtml += '<div class="caption card-body">';
            gridViewHtml += '<h4 class="group card-title inner list-group-item-heading">'+data[i].name+'</h4>';
            gridViewHtml += '<p class="group inner list-group-item-text">'+data[i].size+'&nbsp;';
            if(data[i].rating != undefined){
                for(j=0;j<data[i].rating;j++){
                    gridViewHtml += '<span class="fa fa-star checked"></span>'; 
                }
               
            }
            
            gridViewHtml += '</p>';
            gridViewHtml += '<div class="row"><div class="col-xs-12 col-md-12"><p class="lead">'+data[i].price+'&nbsp;';
            if(data[i].oldPrice != undefined && data[i].oldPrice > data[i].price)
            gridViewHtml += '<strike>'+data[i].oldPrice+'</strike> <span class="checked">You save $'+ (parseInt(data[i].oldPrice.replace('$','')) - parseInt(data[i].price.replace('$','')))+'</span>';
            gridViewHtml += '</p>/div>';

            gridViewHtml += '</div></div></div></div></div>';
        
             $('#products').append(gridViewHtml);
                     }
           
       }
       initialVal = showperscroll;
             showperscroll =showperscroll + 10;


}
$(document).ready(function() {
    $('#list').click(function(event){
    gridorList = 'list';
    event.preventDefault();
    $('#products .item').addClass('list-group-item');
    $('.card').addClass('width100');
    $('#list').addClass('disabled');
    $('#grid').removeClass('disabled');
    });
    $('#grid').click(function(event){
        gridorList = 'grid';
        event.preventDefault();
         $('#products .item').removeClass('list-group-item');
         $('#products .item').addClass('grid-group-item');
         $('.card').removeClass('width100');
         $('#grid').addClass('disabled');
         $('#list').removeClass('disabled');
    });
    $.ajax({
        type: 'GET',
        url: '../src/data/products.json',
        dataType: "json",
        async:false,
        success: function(data) {
            
            productdata = data;
            loadresult(productdata) ; //function call for initial load
        
            
        },
        error: function(jqXHR, textStatus, errorThrown) {
                alert('An error occurred... Look at the console (F12 or Ctrl+Shift+I, Console tab) for more information!');

                $('#result').html('<p>status code: '+jqXHR.status+'</p><p>errorThrown: ' + errorThrown + '</p><p>jqXHR.responseText:</p><div>'+jqXHR.responseText + '</div>');
                console.log('jqXHR:');
                console.log(jqXHR);
                console.log('textStatus:');
                console.log(textStatus);
                console.log('errorThrown:');
                console.log(errorThrown);
            },

            

    });
    $(window).scroll(function() {//lazy loading for performence
      if($(window).scrollTop() == $(document).height() - $(window).height()) {
       // ajax call get data from server and append to the div
         
       loadresult(productdata) ;
        }
    });
});


